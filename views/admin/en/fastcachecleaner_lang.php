<?php

$sLangName  = 'English';

// -------------------------------
// RESOURCE IDENTITFIER = STRING
// -------------------------------

$aLang = array(
    'charset' => 'UTF-8',
    'mxfcc' => 'purge caches',
    'fccStatPartOne' => 'Deletes ',
    'fccStatPartTwo' => ' files in ',
    'fccStatPartThree' => ' directories.',

);
