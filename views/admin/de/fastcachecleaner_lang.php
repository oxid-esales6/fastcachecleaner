<?php

$sLangName  = 'Deutsch';

// -------------------------------
// RESOURCE IDENTITFIER = STRING
// -------------------------------

$aLang = array(
    'charset' => 'UTF-8',
    'mxfcc' => 'Caches leeren',
    'fccStatPartOne' => 'Es wurden ',
    'fccStatPartTwo' => ' Dateien in ',
    'fccStatPartThree' => ' Verzeichnissen gelöscht.',
);
