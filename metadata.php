<?php

/**
 * Metadata version
 */
$sMetadataVersion = '2.0';

/**
 * Module information
 */
$aModule = [
    'id' => 'fastcachecleaner',
    'title' => '<b>f</b>ast <b>c</b>ache <b>c</b>leaner',
    'description' => [
        'de' => 'Löscht die Oxid Caches mit einem Klick.<br><br>Wird in der Hauptnavigation installiert.<br>Nach der Installation bitte <a href="/admin/">hier klicken um das Menü zu aktualisieren und den neuen Punkt sofort sehen zu können.</a><br><br>Weitere Software unter <a href="https://www.petit-souris.de" target="_blank">https://www.petit-souris.de</a> oder <a href="https://gitlab.petit-souris.de" target="_blank">https://gitlab.petit-souris.de</a>, Fragen, Wünsche, Jobangebote gern unter <a href="mailto:oxid@petit-souris.de">oxid@petit-souris.de</a>.<br><a href="https://paypal.me/hpeterseim/1" target=""blank">Und wenn Sie möchten können Sie natürlich auch gerne einen Euro spenden.</a>',
        'en' => 'Deletes Oxid caches with one click.<br><br>Will be installed into the main navigation.<br>Please click <a href="/admin/">here after installation to see the new menue entry.</a><br><br>More peaces of software under <a href="https://www.petit-souris.de" target="_blank">https://www.petit-souris.de</a> or <a href="https://gitlab.petit-souris.de" target="_blank">https://gitlab.petit-souris.de</a>, questions, wishes or job offers goes to <a href="mailto:oxid@petit-souris.de">oxid@petit-souris.de</a>.<br><a href="https://paypal.me/hpeterseim/1" target=""blank">Spend an euro if you want.</a>'
    ],
    'thumbnail' => 'views/admin/image.php',
    'version' => '1.0.7',
    'author' => 'Hannes Peterseim',
    'email' => 'oxid@petit-souris.de',
    'url' => 'https://www.petit-souris.de',

    'events' => [
        'onActivate'   => '\hpeterseim\fastcachecleaner\src\core\fastcachecleanerevents::onActivate',
        'onDeactivate' => '\hpeterseim\fastcachecleaner\src\core\fastcachecleanerevents::onDeactivate'
    ],

    'controllers' => [
        'fastcachecleaner' => hpeterseim\fastcachecleaner\src\controller\admin\fastcachecleaner::class,
    ],

    'extend' => [],

    'templates' => [],

    'blocks' => [
        ['template' => 'header.tpl', 'block'=>'admin_header_links', 'file'=>'/views/admin/blocks/navinclude.tpl']
    ],

    'settings' => []
];

