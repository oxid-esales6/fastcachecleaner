# fcc alias fast cache cleaner

## Installation :
1. Geben Sie auf der Konsole den Befehl 'composer config repositories.hpeterseim/fastcachecleaner git https://gitlab.petit-souris.de/oxid-esales6/fastcachecleaner.git' ein um composer mit dem Modul bekannt zu machen.
2. geben Sie nun 'composer require hpeterseim/fastcachecleaner:"dev-master"' und installieren Sie so das Modul.
3. Gehen Sie im Backend in den Bereich Erweiterungen (bei Oxid EE im jeweiligen Subshop) und aktivieren Sie das Modul.
4. Klicken Sie auf den Link in der Beschreibung oder drücken Sie F5 um den Browser zu aktualisieren. Die Funktion findet sich rechts in der Hauptnavigation ('Caches leeren').
