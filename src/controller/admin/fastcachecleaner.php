<?php
/**
 * Copyright 2019 by Hannes Peterseim
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Questions, job offers or simply wishes for this product are welcome, please use oxid@petit-souris.de or take a look @ https://gitlab.petit-souris.de for other peaces of software.
 */

namespace hpeterseim\fastcachecleaner\src\controller\admin;

use OxidEsales\Eshop\Core\Registry;

/**
 * Class fast_cache_cleaner
 */
class fastcachecleaner extends \OxidEsales\Eshop\Application\Controller\Admin\AdminDetailsController
{

    /**
     * @return string
     */
    public function render() {
        $this->cleanCache(false);
        $this->_sThisTemplate = 'header.tpl';
        parent::render();

        return $this->_sThisTemplate;
    }

    /**
     * cleans the smarty caches itself
     *
     * @return void
     */
    function cleanCache($fromInstall = false)
    {
        $inDirs = 0;
        $killedFiles = 0;
        $basedir = str_replace('//','/',str_replace('out','', Registry::getConfig()->getOutDir()));
        $dirs = array('tmp/','tmp/smarty/');
        foreach($dirs as $dir){
            $files = glob($basedir . $dir . '*');
            foreach($files as $file) {
                if (is_file($file)) {
                    unlink($file);
                    $killedFiles++;
                }
            }
            $inDirs++;
        }
        if ($fromInstall == false) {
            $this->_aViewData['killedFiles'] = $killedFiles;
            $this->_aViewData['inDirs'] = $inDirs;
        }
    }

}
